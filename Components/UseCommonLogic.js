import React, {useState, useEffect} from "react";


const useCommonLogic = (initialFieldValues) => {
    
const [data, setData] = useState(initialFieldValues);

const handleInputChange = e => {
    const { email, value } = e.target;
    setData({
      ...data,
      [email]: value
    });
  };

    return (
        data,
        setData,
        handleInputChange
    ) ;
}

export default useCommonLogic;