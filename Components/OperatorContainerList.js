import React, { useState } from "react";
import {
  Button,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Dimensions
} from "react-native";
import CardView from "./CardView";
import Colors from "./Constants/Colors";
import * as Animatable from "react-native-animatable";

const OperatorContainerList = props => {
  return (
    <TouchableWithoutFeedback>
      <CardView style={styles.operatorContainer}>
      <Animatable.Image
        animation="bounceIn"
        duraton="1500"
        source={require("../assets/OPTUS.png")}
        style={styles.operatorimage}
        resizeMode="stretch"
      />
      </CardView>
    </TouchableWithoutFeedback>
  );
};

const { height } = Dimensions.get("screen");
const height_logo = height * 0.08;
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center"
  },
  title: {
    fontSize: 20,
    marginVertical: 10
  },
  inputContainer: {
    width: 300,
    maxWidth: "80%",
    alignItems: "center"
  },
  buttonContainer: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    paddingHorizontal: 15
  },
  button: {
    width: 100
  },
  input: {
    width: 150,
    height: 30,
    textAlign: "center",
    fontSize: 22
  },
  summaryContainer: {
    marginTop: 20,

    width: 350,
    backgroundColor: "silver"
  },
  operatorContainer: {
    marginTop: 10,
    width: 90,
    height: 90,
     backgroundColor:"silver"
  },
  operatorimage: {
    width: height_logo,
    height: height_logo
  }
});

export default OperatorContainerList;
