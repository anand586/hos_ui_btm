import React, {useState, useEffect} from "react";
import { View, Text, Button, StyleSheet, TextInput } from "react-native";
import {connect} from "react-redux";
import * as actions from "../Actions/RegisterAction";

const Register = (props) => {

    useEffect(() => {
        props.fetchAllRegistration ()
    }, [])


  return (
    <View>
      <Text>
        Get API Data of Registered Users
        {/* <Text>{props.registrationList.map((record, index) => {
                return (<TableRow>
                    <TableCell>{record.firstName}</TableCell>
                </TableRow>)
            })}</Text> */}
      </Text>
    </View>
  );
};



const mapStateToProps = state => ({
    registrationList: state.RegisterReducer.list
})


const mapActionToProps = {
    fetchAllRegistration:actions.fetchAll
}

export default connect(mapStateToProps,mapActionToProps)(Register);
