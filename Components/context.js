import React from "react";
// import { createContext } from "istanbul-lib-report";

import { store } from "../Actions/store";
import { Provider } from "react-redux";
import Register from "./RegisterComponent";
import RegisterScreen from "../Screens/RegisterScreen";

export const AuthContext = React.createContext();

export const ProviderContext = () => (
  <Provider store={store}>
    {/* <Register /> */}
    <RegisterScreen />
  </Provider>
);
