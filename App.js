import "react-native-gesture-handler";
import React, { useState, useEffect, useMemo, useReducer } from "react";
import { StyleSheet, ActivityIndicator } from "react-native";
import { NavigationContainer, useFocusEffect } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import MainTabScreen from "./Screens/MainTabScreen";
import { DrawerContent } from "./Screens/DrawerContent";
import WalletScreen from "./Screens/WalletScreen";
import LoginScreen from "./Screens/LoginScreen";

import RootStackScreen from "./Screens/RootStackScreen";
import { View } from "react-native-animatable";

import { AuthContext, ProviderContext } from "./Components/context";
import { Provider } from "react-redux";
import { store } from "./Actions/store";

import AsyncStorage from "@react-native-community/async-storage";
import SplashScreen from "./Screens/SplashScreen";
import RegisterScreen from "./Screens/RegisterScreen";

const Drawer = createDrawerNavigator();

export default function App() {
  // const [isLoading, setIsLoading] = useState(true);
  // const [userToken, setUserToken] = useState(null);

  const initialLoginState = {
    isLoading: true,
    userName: null,
    userToken: null
  };

  const loginReducer = (prevState, action) => {
    switch (action.type) {
      case "RETRIEVE_TOKEN":
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false
        };
      case "LOGIN":
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          isLoading: false
        };
      case "LOGOUT":
        return {
          ...prevState,
          userName: null,
          userToken: null,
          isLoading: false
        };
      case "REGISTER":
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          isLoading: false
        };
    }
  };

  const [loginState, dispatch] = useReducer(loginReducer, initialLoginState);

  const authContext = useMemo(
    () => ({
      signIn: async (userName, password) => {
        // setUserToken("fgkj");
        // setIsLoading(false);
        let userToken;
        userToken = null;
        if (userName == "user" && password == "pass") {
          try {
            userToken = "anand";
            await AsyncStorage.setItem("userToken", userToken);
          } catch (e) {
            console.log(e);
          }
        }
        dispatch({ type: "LOGIN", id: userName, token: userToken });
      },
      signOut: async () => {
        try {
          // userToken = "anand";
          await AsyncStorage.removeItem("userToken");
        } catch (e) {
          console.log(e);
        }
        dispatch({ type: "LOGOUT" });
      },
      signUp: () => {
        // setUserToken("fgkj");
        // setIsLoading(false);
        //dispatch({ type: "LOGOUT" })
      }
    }),
    []
  );

  useEffect(() => {
   //console.log( props.mapActionToLogin.detailsLogin)
    setTimeout(async () => {
      // setIsLoading(false);
      let userToken;
      userToken = null;
      try {
        userToken = await AsyncStorage.getItem("userToken");
      } catch (e) {
        console.log(e);
      }
      dispatch({ type: "REGISTER", token: userToken });
    }, 1000);
  }, []);

  if (loginState.isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return (

      <Provider store={store}>
        <NavigationContainer>
        
            <Drawer.Navigator
              drawerContent={props => <DrawerContent {...props} />}
            >
              <Drawer.Screen name="Splash" component={SplashScreen} />
              <Drawer.Screen name="LoginScreen" component={LoginScreen} />
              <Drawer.Screen name="MainTabScreen" component={MainTabScreen} />
              <Drawer.Screen name="RegisterScreen" component={RegisterScreen}/>
              <Drawer.Screen name="WalletScreen" component={WalletScreen}/>
            </Drawer.Navigator>
   
        </NavigationContainer>
      </Provider>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
