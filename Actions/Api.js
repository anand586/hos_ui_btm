import axios from "axios";

const baseUrl = "http://192.168.1.102/api/";

export default {
    registration(url=baseUrl + "Registration/"){
        return{
            fetchAll: () => axios.get(url + "GetRegistrations"),
            fetchById: id => axios.get(url+id),
            create:newRecord => axios.post(url+"SaveRegistration",newRecord),
            update:(id, updateRecord) => axios.put(url+id, updateRecord),
            delete:id => axios.delete(url+id)
        }
    },
    login(url=baseUrl + "Security/"){
        return{
            fetchAll: () => axios.get(url + "GetRegistrations"),
            fetchById: id => axios.get(url+id),
            create:newRecord => axios.post(url+"Login",newRecord),
            update:(id, updateRecord) => axios.put(url+id, updateRecord),
            delete:id => axios.delete(url+id)
        }
    }
}