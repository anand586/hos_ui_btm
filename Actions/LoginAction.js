import Api from "./Api";

export const ACTION_TYPES = {
  CREATE: "CREATE",
  UPDATE: "UPDATE",
  DELETE: "DELETE",
  FETCH_ALL: "FETCH_ALL"
};

const formatData = data => ({
  ...data
});

export const fetchAll = () => dispatch => {
  //get data
  Api.registration()
    .fetchAll()
    .then(response => {
      console.log(response);
      dispatch({
        type: ACTION_TYPES.FETCH_ALL,
        payload: response.data
      });
    })
    .catch(err => console.log(err));
};

export const create = (data, onSuccess) => dispatch => {
  data = formatData(data);
  Api.login()
    .create(data)
    .then(response => {
      dispatch({
        type: ACTION_TYPES.CREATE,
        payload: response.data
      });
      console.log(response);
      onSuccess();
    })
    .catch(err => console.log(err));
};
