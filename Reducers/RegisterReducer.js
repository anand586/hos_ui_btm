import { ACTION_TYPES } from "../Actions/RegisterAction";

const initialState = {
  list: []
};

export const RegisterReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.FETCH_ALL:
      return {
        ...state,
        list: [...action.payload]
      };

    case ACTION_TYPES.CREATE:
      return {
        ...state,
        list: [...state.list, action.payload]
      };
    default:
      return state;
  }
};
