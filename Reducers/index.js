import {combineReducers, applyMiddleware, compose} from "redux";
import {RegisterReducer} from "./RegisterReducer";
import {LoginReducer} from "./LoginReducer";

export const reducers = combineReducers({
    RegisterReducer,LoginReducer
})
