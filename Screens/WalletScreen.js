import React from "react";
import { View, Text, TextInput, StyleSheet, Button } from "react-native";

const WalletScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Text>Wallet Screen</Text>
            <Button title="Home" onPress={() => {navigation.navigate("Home")} } />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    }
});

export default WalletScreen;