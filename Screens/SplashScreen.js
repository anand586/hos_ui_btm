import React from "react";
import { View, Image, Text, Button, StyleSheet, TextInput, Dimensions, TouchableOpacity } from "react-native";

import { LinearGradient } from 'expo-linear-gradient';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import * as Animatable from "react-native-animatable";

const SplashScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Animatable.Image animation="bounceIn" duraton="1500" source={require("../assets/HKSkincarelogo.png")}
                    style={styles.logo}
                    resizeMode="stretch"
                />
            </View>
            <Animatable.View style={styles.footer} animation="fadeInUpBig">
                <Text style={styles.title}>Pay your Bills Anywhere Anytime!</Text>
                <Text style={styles.text}>Sign In With Account</Text>
                <View style={styles.button}>
                <TouchableOpacity onPress={() => navigation.navigate("LoginScreen")}>
                 <LinearGradient colors={["#08d4c4","#01ab9d"]}
                 style={styles.signIn}
                 >
                     <Text style={styles.textSign}>Get Started</Text>
                     <MaterialIcons name="navigate-next"
                     color="#fff"
                     size={20}
                     />
                 </LinearGradient>
                </TouchableOpacity>
                </View>
              
            </Animatable.View>
        </View>
    );
};

const { height } = Dimensions.get("screen");
const height_logo = height * 0.15;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#009387"
    },
    header: {
        flex: 2,
        justifyContent: "center",
        alignItems: "center"
    },
    footer: {
        flex: 1,
        backgroundColor: "#fff",
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 80,
        paddingHorizontal: 20
    },
    logo: {
        width: height_logo,
        height: height_logo
    },   
    title: {
        color: "#05375a",
        fontSize:30,
        fontWeight:"bold"
    },
    text: {
        color: "grey",
        marginTop: 6
    },
    button: {
        alignItems: "flex-end",
        marginTop: 50
    },
    signIn: {
        width: 150,
        height: 40,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20,
        flexDirection: "row"
    },
    textSign: {
        color: "white",
        fontWeight: "bold"
    }
});

export default SplashScreen;