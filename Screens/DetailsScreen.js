import React from "react";
import { Button, View, Text, TextInput, StyleSheet } from "react-native";


const DetailsScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Button title="Home" onPress={() => { navigation.navigate("Home") }} />
            <Button title="DetailsLoad" onPress={() => { navigation.push("Details") }} />
            <Button title="Go Back" onPress={() => { navigation.goBack() }} />
            <Button title="Go First" onPress={() => { navigation.popToTop() }} />
        </View>
    );
};

const styles = StyleSheet.create({

});

export default DetailsScreen;
