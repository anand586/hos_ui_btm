import React, {useEffect} from "react";
import { Button, View, StyleSheet, TextInput } from "react-native";
import { Avatar, Title, Caption, Paragraph, Drawer, Text, TouchableRipple, Switch } from "react-native-paper";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MainTabScreen from "./MainTabScreen";

import {AuthContext} from "../Components/context";

export function DrawerContent(props) {

    const [isDarkTheme, setIsDarkTheme] = React.useState(false);
    const [isLocate, setIsLocate] = React.useState(false);

// const {signOut} = React.useContext(AuthContext);

    const toggleTheme = () => {
        setIsDarkTheme(!isDarkTheme);
        setIsLocate(!isLocate);
    }

    const loginDetails = () => {
        console.log("reach3")
       console.log( props.MainTabScreen.loginDetails);
    }

    useEffect(() => {
        loginDetails
         });

    return (
        <View style={{ flex: 1 }}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{ flexDirection: "row", marginTop: 15 }}>
                            <Avatar.Image
                                source={{ uri: "https://lh3.googleusercontent.com/proxy/GtRzoVc2yqugYVyzNu9CkpCdY8JU2E6vXOeJeij1YoC_VBljbT_yMQzqNC8AEFSquFcPEZhPJ2H9EnXKC5RhesY" }}
                                size={50}
                            />
                            <View style={{ marginLeft: 15, flexDirection: "column" }}>
                                <Title style={styles.title}>HealthKNOCKs</Title>
                                <Caption style={styles.caption}>@OneStopStore</Caption>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>80</Paragraph>
                                <Caption style={styles.caption}>following</Caption>
                            </View>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>100</Paragraph>
                                <Caption style={styles.caption}>followers</Caption>
                            </View>
                        </View>
                    </View>

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem icon={({ color, size }) => (
                            <Icon name="home-outline"
                                color={color}
                                size={size}
                            />
                        )}
                            label="Home"
                            onPress={() => { props.navigation.navigate("Home")}}
                        />

                        <DrawerItem icon={({ color, size }) => (
                            <Icon name="store"
                                color={color}
                                size={size}
                            />
                        )}
                            label="Stores"
                            onPress={() => {props.navigation.navigate("Store") }}
                        />

                        <DrawerItem icon={({ color, size }) => (
                            <Icon name="wallet-outline"
                                color={color}
                                size={size}
                            />
                        )}
                            label="Wallet"
                            onPress={() => {props.navigation.navigate("WalletScreen")  }}
                        />

                        <DrawerItem icon={({ color, size }) => (
                            <Icon name="account-check-outline"
                                color={color}
                                size={size}
                            />
                        )}
                            label="Support"
                            onPress={() => { }}
                        />

                        <DrawerItem icon={({ color, size }) => (
                            <Icon name="bookmark-outline"
                                color={color}
                                size={size}
                            />
                        )}
                            label="Bookmarks"
                            onPress={() => { }}
                        />
                    </Drawer.Section>
                    <Drawer.Section title="Preferences">
                        <TouchableRipple onPress={() => { toggleTheme() }}>
                            <View style={styles.preference}>
                                <Text>Locate Stores NearBy</Text>
                                <View pointerEvents="none">
                                    <Switch value={isLocate} />
                                </View>
                            </View>
                            {/* <View style={styles.preference}>
                              <Text>Dark Theme</Text>
                              <View pointerEvents="none">
                              <Switch value={isDarkTheme}/>
                              </View>                            
                          </View> */}
                        </TouchableRipple>
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem icon={({ color, size }) => (
                    <Icon name="exit-to-app" color={color} size={size} />
                )}
                    label="Sign Out" onPress={() => { signOut()}} />
            </Drawer.Section>
        </View>
    );
}



const styles = StyleSheet.create({
    drawerContent: {
        flex: 1
    },
    userInfoSection: {
        paddingLeft: 20
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: "bold"
    },
    caption: {
        fontSize: 14,
        lineHeight: 14
    },
    row: {
        marginTop: 20,
        flexDirection: "row",
        alignItems: "center"
    },
    section: {
        flexDirection: "row",
        alignItems: "center",
        marginRight: 15
    },
    paragraph: {
        fontWeight: "bold",
        marginRight: 3
    },
    drawerSection: {
        marginTop: 15
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: "#f4f4f4",
        borderTopWidth: 1
    },
    preference: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingVertical: 12,
        paddingHorizontal: 16
    }
});

export default DrawerContent;