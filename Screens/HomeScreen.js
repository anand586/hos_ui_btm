import React, { useState } from "react";
import {
  Image,
  Button,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Dimensions,
  ScrollView
} from "react-native";
import { TouchableRipple, Switch, RadioButton } from "react-native-paper";
import CardView from "../Components/CardView";
import Input from "../Components/Input";
import NumberContainer from "../Components/NumberContainer";
import Colors from "../Components/Constants/Colors";
import * as Animatable from "react-native-animatable";
import OperatorContainerList from "../Components/OperatorContainerList";

const HomeScreen = ({ navigation }) => {
  const [value, SetValue] = useState("");
  const [confirm, setConfirm] = useState(false);
  const [selectNumber, setSelectnumber] = useState(false);

  const [isDarkTheme, setIsDarkTheme] = React.useState(false);
  const [isPrepaid, setIsPrepaid] = React.useState(false);
     const [isPostpaid, setIsPostpaid] = React.useState(false);


  // const {signOut} = React.useContext(AuthContext);

  const toggleThemePrepaid = () => {
    setIsDarkTheme(!isDarkTheme);
    setIsPrepaid(!isPrepaid);
  };

  const toggleThemePostpaid = () => {
    setIsDarkTheme(!isDarkTheme);
    setIsPostpaid(!isPostpaid);
  };

  const numberInputHnadler = inputText => {
    SetValue(inputText.replace(/[^0-9]/g, ""));
  };

  const resetInputandler = () => {
    SetValue("");
    setConfirm(false);
  };

  const confirmInputHandler = () => {
    const chosenNumber = parseInt(value);
    if (isNaN(chosenNumber) || chosenNumber <= 0) {
      Alert.alert("Invalid Number", "Number has to be valid Mobile number", [
        { text: "OKAY", style: "destructive", onPress: resetInputandler }
      ]);
      return;
    }
    setConfirm(true);
    setSelectnumber(chosenNumber);
    SetValue("");
    Keyboard.dismiss();
  };

  let confirmedOutput;
  if (confirm) {
    confirmedOutput = (
      <CardView style={styles.summaryContainer}>
        <Text style={{ fontSize: 15, textAlign: "left" }}>
          Select Operators for: {isPostpaid}
        </Text>
        <NumberContainer>{selectNumber}</NumberContainer>
        <OperatorContainerList />
        <Button
          title="Proceed"
          onPress={() => props.onStartGame(selectNumber)}
        />
      </CardView>
    );
  }

  return (
    <ScrollView>
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}
      >
        <View style={styles.screen}>
          <Text style={styles.title}>Recharge/Pay Bills</Text>
          <CardView style={styles.inputContainer}>
            <View style={styles.toggleContainer}>
            <View style={styles.toggle}>
              <TouchableRipple
                onPress={() => {
                    toggleThemePostpaid();
                }}
              >
                <View style={{flexDirection:"row"}}>
                  <Text style={styles.title}>Postpaid</Text>
                  <View pointerEvents="none" style={{paddingTop:5, paddingLeft:10}}>
                    <Switch value={isPostpaid} />
                  </View>
                </View>
              </TouchableRipple>
            </View>
            <View style={styles.toggle}>
              <TouchableRipple
                onPress={() => {
                    toggleThemePrepaid();
                }}
              >
                <View style={{flexDirection:"row"}}>
                  <Text style={styles.title}>Prepaid</Text>
                  <View pointerEvents="none" style={{paddingTop:5, paddingLeft:10}}>
                    <Switch value={isPrepaid} />
                  </View>
                </View>
              </TouchableRipple>
            </View>
            </View>
            <View style={{ flexDirection: "row", paddingTop: 10 }}>
              <Input
                style={styles.input}
                placeholder="Enter Mobile Number"
                blurOnSubmit
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="number-pad"
                maxLength={9}
                onChangeText={numberInputHnadler}
                value={value}
              />
            </View>
            <View style={styles.buttonContainer}>
              <View style={styles.button}>
                <Button title="RESET" onPress={resetInputandler} />
              </View>
              <View style={styles.button}>
                <Button title="CONFIRM" onPress={confirmInputHandler} />
              </View>
            </View>
          </CardView>
          {confirmedOutput}
        </View>
      </TouchableWithoutFeedback>
    </ScrollView>
  );
};

const { height } = Dimensions.get("screen");
const height_logo = height * 0.065;
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center"
  },
  title: {
    fontSize: 20,
    marginVertical: 10
  },
  inputContainer: {
    width: 350,
    alignItems: "center"
  },
  buttonContainer: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    paddingHorizontal: 15
  },
  button: {
    width: 100
  },
  toggleContainer: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    paddingHorizontal: 15
  },
  toggle: {
      
    width: 100
  },
  input: {
    width: 300,
    height: 30,
    textAlign: "justify",
    fontSize: 23
  },
  summaryContainer: {
    marginTop: 20,
    width: 350
    //  backgroundColor: "silver"
  },
  operatorContainer: {
    marginTop: 10,
    width: 80,
    height: 80
    // backgroundColor:"silver"
  },
  operatorimage: {
    width: height_logo,
    height: height_logo
  },
  preference: {
    flexDirection: "row",
    textAlign: "justify",

    paddingVertical: 15,
    paddingHorizontal: 15
  }
});

export default HomeScreen;
