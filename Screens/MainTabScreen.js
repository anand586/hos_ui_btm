import React,{useEffect} from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from "react-native-vector-icons/Ionicons";

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import HomeScreen from "./HomeScreen";
import LoginScreen from "./LoginScreen";
import RegisterScreen from "./RegisterScreen";
import DetailsScreen from "./DetailsScreen";
import StoreScreen from "./StoreScreen";

const HomeStack = createStackNavigator();
const DetailsStack = createStackNavigator();
const LoginStack = createStackNavigator();
const RegisterStack = createStackNavigator();
const StoreStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();



const MainTabScreen = ({navigation, ...props}) => {
    // useEffect(() => {
    //     console.log(props.record)
    //     this.props.navigation.state.params.jsonData
    //    }, []);
    return(
<Tab.Navigator
      initialRouteName="Home"
      activeColor="#fff"
      barStyle={{ backgroundColor: "#009387" }}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Store"
        component={StoreStackScreen}                         
        options={{
          tabBarLabel: 'Store',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="store" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={LoginStackScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
    )
};

const HomeStackScreen = ({ navigation }) => (
    <HomeStack.Navigator screenOptions={{
        headerStyle: {
            backgroundColor: "#009387"
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
            fontWeight: "bold"
        }
    }}>
        <HomeStack.Screen name="Home" component={HomeScreen} options={{
            title: "HealthKNOCKS",
            headerLeft: () => (
                <Icon.Button name="ios-menu" size={25}
                    backgroundColor="#009387" onPress={() => { navigation.openDrawer() }}></Icon.Button>
            )
        }} />
        {/* <HomeStack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
          <Stack.Screen name="Details" component={DetailsScreen} /> */}
    </HomeStack.Navigator>
);


const DetailsStackScreen = ({ navigation }) => (
    <DetailsStack.Navigator screenOptions={{
        headerStyle: {
            backgroundColor: "#009387"
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
            fontWeight: "bold"
        }
    }}>
        <DetailsStack.Screen name="Details" component={DetailsScreen} options={{
            title: "Stores",
            headerLeft: () => (
                <Icon.Button name="ios-menu" size={25}
                    backgroundColor="#009387" onPress={() => { navigation.openDrawer() }}></Icon.Button>
            )
        }} />
        {/* <HomeStack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Register" component={RegisterScreen} />
            <Stack.Screen name="Details" component={DetailsScreen} /> */}
    </DetailsStack.Navigator>
);

const LoginStackScreen = ({ navigation }) => (
    <LoginStack.Navigator screenOptions={{
        headerStyle: {
            backgroundColor: "#009387"
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
            fontWeight: "bold"
        }
    }}>
        <LoginStack.Screen name="Login" component={LoginScreen} options={{
            title: "SignIn",
            headerLeft: () => (
                <Icon.Button name="ios-menu" size={25}
                    backgroundColor="#009387" onPress={() => { navigation.openDrawer() }}></Icon.Button>
            )
        }} />
    </LoginStack.Navigator>
);



const RegisterStackScreen = ({ navigation }) => (
    <RegisterStack.Navigator screenOptions={{
        headerStyle: {
            backgroundColor: "#009387"
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
            fontWeight: "bold"
        }
    }}>
        <RegisterStack.Screen name="SignUp" component={RegisterScreen} options={{
            title: "SignUp",
            headerLeft: () => (
                <Icon.Button name="ios-menu" size={25}
                    backgroundColor="#009387" onPress={() => { navigation.openDrawer() }}></Icon.Button>
            )
        }} />
    </RegisterStack.Navigator>
);

const StoreStackScreen = ({ navigation }) => (
    <StoreStack.Navigator screenOptions={{
        headerStyle: {
            backgroundColor: "#009387"
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
            fontWeight: "bold"
        }
    }}>
        <StoreStack.Screen name="Store" component={StoreScreen} options={{
            title:"Store",
            headerLeft: () => (
                <Icon.Button name="ios-menu" size={25}
                    backgroundColor="#009387" onPress={() => { navigation.openDrawer() }}></Icon.Button>
            )
        }} />
    </StoreStack.Navigator>
);


export default MainTabScreen;