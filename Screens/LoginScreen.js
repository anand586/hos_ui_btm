import React, { useState, useEffect } from "react";
import {
    Button, View, Text, TextInput, StyleSheet, Dimensions, TouchableOpacity, Platform,
    Keyboard, StatusBar
} from "react-native";

import { LinearGradient } from 'expo-linear-gradient';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import * as Animatable from "react-native-animatable";

import { AuthContext } from "../Components/context";
import { connect } from "react-redux";
import * as actions from "../Actions/LoginAction";

const LoginScreen = ({ navigation, ...props }) => {
    //toggle eye lock
    const [data, setData] = useState({
        emailId: "",
        password: "",
        check_textInputChange: false,
        secureTextEntry: true
    });

    // const { signIn } = React.useContext(AuthContext);

    const userInputChange = (val) => {
        if (val.length !== 0) {
            setData({
                ...data,
                emailId: val,
                check_textInputChange: true
            })
        }
        else {
            setData({
                ...data,
                emailId: val,
                check_textInputChange: false
            })
        }
    };

    const passwordTextChanged = (val) => {
        setData({
            ...data,
            password: val
        });
    };

    const updateSecurityTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    };

    const loginHandle = (userName, password) => {
         
       props.createLogin(data);
       props.loginList.map((record) => {
        if(record.userName!==null){
            console.log(record.userName)
            navigation.navigate("MainTabScreen", {jsonData:record.userName});
        }
       })
    }

    //  const loginDetails = (
    //      props.loginList.map((record) => {
    //         if(record.userName!==null){
    //             navigation.navigate("LoginScreen", { screen: "MainTabScreen" });
    //         }
    //        })
    // );

    // useEffect(() => {
    //     {loginDetails}
    //     console.log("reach")
    // },[])

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#009387" barStyle="light-content" />
            <View style={styles.header}>
                <Text style={styles.text_header}>Welcome</Text>
            </View>
            <Animatable.View style={styles.footer}
                animation="fadeInUpBig"
            >
                <Text style={styles.text_footer}>Email Address</Text>
                <View style={styles.aciton}>
                    <FontAwesome name="user-o" color="#05375a" size={20} />
                    <TextInput placeholder="your email id" style={styles.TextInput}
                        autoCapitalize="none" onChangeText={(val) => userInputChange(val)} />

                    {
                        data.check_textInputChange ?
                            <Animatable.View animation="bounceIn">
                                <Feather name="check-circle" color="green" size={20} />
                            </Animatable.View>
                            : null
                    }
                </View>

                <Text style={[styles.text_footer, { marginTop: 35 }]}>Password</Text>
                <View style={styles.aciton}>
                    <Feather name="lock" color="#05375a" size={20} />
                    <TextInput placeholder="your password" style={styles.TextInput}
                        autoCapitalize="none" secureTextEntry={data.secureTextEntry ? true : false}
                        onChangeText={(val) => passwordTextChanged(val)}
                    />
                    <TouchableOpacity onPress={updateSecurityTextEntry}>
                        {
                            data.secureTextEntry ?
                                <Feather name="eye-off" color="green" size={20} />
                                :
                                <Feather name="eye" color="green" size={20} />
                        }
                    </TouchableOpacity>
                </View>


                <TouchableOpacity>
                    <Text style={{color:"#009387", marginTop:15}}>Forgot Password?</Text>
                </TouchableOpacity>
                <View style={styles.button}>
                    <TouchableOpacity style={styles.signIn} onPress={() => { loginHandle(data.emailId, data.password) }} >
                        <LinearGradient colors={["#08d4c4", "#01ab9d"]}
                            style={styles.signIn}
                        >
                            <Text style={[styles.textSign, { color: "#fff" }]}>Sign In</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => navigation.navigate("RegisterScreen")}
                        style={[styles.signIn, { borderColor: "#009387", borderWidth: 1, marginTop: 15 }]}
                    >
                        <Text>Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </Animatable.View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#009387"
    },
    header: {
        flex: 1,
        justifyContent: "flex-end",
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: "#fff",
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 30
    },
    text_footer: {
        color: "#05375a",
        fontSize: 18
    },
    aciton: {
        flexDirection: "row",
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: "#f2f2f2",
        paddingBottom: 5
    },
    TextInput: {
        flex: 1,
        marginTop: Platform.OS === "ios" ? 0 : -12,
        paddingLeft: 10,
        color: "#05375a"
    },
    button: {
        alignItems: "center",
        marginTop: 50
    },
    signIn: {
        width: "100%",
        height: 50,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 10
    },
    textSign: {
        fontSize: 18,
        fontWeight: "bold"
    }
});

const mapStateToProps = state => ({
    loginList: state.LoginReducer.list
  });
  
  const mapActionToProps = {
    createLogin: actions.create
  };

export default connect(mapStateToProps,mapActionToProps)(LoginScreen);